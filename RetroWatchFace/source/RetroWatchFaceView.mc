using Toybox.WatchUi;
using Toybox.Graphics;
using Toybox.System;
using Toybox.Lang;
using Toybox.Time.Gregorian;
using Toybox.Activity;
using Toybox.Attention;


class RetroWatchFaceView extends WatchUi.WatchFace {

    private var screenWidth;
    private var screenHeight;
	private var largeFont = null; 
    private var mediumFont = null;
    private var iconsFont = null;

    private var x;
    private var y;

    private var heartRate;
    private var stepCount;
    private var battery;
    private var batteryDays;
    private var date;
    private var dateString;
    private var clockTime;
    private var timeString;

    private var displayColor;

    private var sleeping;

    function initialize() {
        WatchFace.initialize();

        largeFont = WatchUi.loadResource(Rez.Fonts.largefont);
        mediumFont = WatchUi.loadResource(Rez.Fonts.mediumfont);
        iconsFont = WatchUi.loadResource(Rez.Fonts.iconfont);

        x = 0;
        y = 0;
        sleeping = false;
        displayColor = Graphics.COLOR_WHITE;
    }

    // Load your resources here
    function onLayout(dc) {
        setLayout(Rez.Layouts.WatchFace(dc));

        screenWidth = dc.getWidth();
        screenHeight = dc.getHeight();
    }

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() {
    }

    // Update the view
    function onUpdate(dc) {
        View.onUpdate(dc);

        if (sleeping) {
            drawLowPower(dc);
        } else {
            drawHighPower(dc);
        }
    }

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() {
    }

    // The user has just looked at their watch. Timers and animations may be started here.
    function onExitSleep() {
        sleeping = false;
        displayColor = Graphics.COLOR_WHITE;
        WatchUi.requestUpdate();
    }

    // Terminate any active timers and prepare for slow updates.
    function onEnterSleep() {
        sleeping = true;
        displayColor = Graphics.COLOR_LT_GRAY;
        WatchUi.requestUpdate();
    }
    
    private function drawLowPower(dc) {
        drawHoursMinutes(dc);
    }

    private function drawHighPower(dc) {
        // drawBorder(dc);
        drawHoursMinutes(dc);
        drawDateDisplay(dc);
        drawStepCountDisplay(dc);
        drawHeartRateDisplay(dc);
        drawBatteryDisplay(dc);
    }

    private function drawBorder(dc) {
        // Draw circular border
        x = screenWidth / 2;

        dc.setColor(displayColor, Graphics.COLOR_TRANSPARENT);
        dc.drawCircle(x,x,x-4);
    }

	private function drawDateDisplay(dc) {        
		date = Gregorian.info(Time.now(), Time.FORMAT_LONG); 
        dateString = Lang.format("$1$ $2$ $3$", [date.day_of_week, date.day, date.month]);

        x = screenWidth / 2;
        y = screenHeight / 4;

        dc.setColor(displayColor, Graphics.COLOR_TRANSPARENT);
        dc.drawText(
            x,
            y,
            mediumFont,
            dateString,
            Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER
        );
	}

    private function drawHoursMinutes(dc) {
        clockTime = System.getClockTime();
        timeString = Lang.format("$1$:$2$", [(clockTime.hour % 12 == 0) ? 12 : clockTime.hour % 12, clockTime.min.format("%02d")]);

        var second = clockTime.sec;

        x = screenWidth / 2;
        y = screenHeight / 2;

        dc.setColor(displayColor, Graphics.COLOR_TRANSPARENT);
        dc.drawText(
            x,
            y + 15,
            largeFont,
            timeString,
            Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER
        );
    }

    private function drawBatteryDisplay(dc) {
    	battery = System.getSystemStats().battery.toNumber();
        batteryDays = System.getSystemStats().batteryInDays;
        var batteryIcon;
        var batteryColor;

        if (batteryDays < 0.5) {
            batteryDays = 0;
        } else {
            batteryDays = Math.ceil(batteryDays).toNumber();
        }

        x = screenWidth / 2;
        y = screenHeight / 4 - 50;
        var padding = 5;

        if (battery < 5) {
            batteryIcon = "e";
            batteryColor = Graphics.COLOR_RED;
        } else if (battery < 25) {
            batteryIcon = "c";
            batteryColor = Graphics.COLOR_RED;
        } else if (battery < 50) {
            batteryIcon = "d";
            batteryColor = Graphics.COLOR_YELLOW;
        } else if (battery < 75) {
            batteryIcon = "b";
            batteryColor = Graphics.COLOR_GREEN;
        } else {
            batteryIcon = "a";
            batteryColor = Graphics.COLOR_GREEN;
        }

        dc.setColor(batteryColor, Graphics.COLOR_TRANSPARENT);
        dc.drawText(
            x,
            y,
            iconsFont,
            batteryIcon,
            Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER
        );
        dc.setColor(displayColor, Graphics.COLOR_TRANSPARENT);
        dc.drawText(
            x,
            y,
            iconsFont,
            "e",
            Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER
        );

        dc.drawText(
            x-padding,
            y,
            mediumFont,
            Lang.format("$1$ ", [batteryDays]),
            Graphics.TEXT_JUSTIFY_RIGHT | Graphics.TEXT_JUSTIFY_VCENTER
        );

        dc.drawText(
            x+padding,
            y,
            mediumFont,
            Lang.format(" $1$%", [battery]),
            Graphics.TEXT_JUSTIFY_LEFT | Graphics.TEXT_JUSTIFY_VCENTER
        );
    }
    
    private function drawStepCountDisplay(dc) {
        x = screenWidth / 4 * 3 - 30;
        y = screenHeight / 4 * 3;

        var stepCount = ActivityMonitor.getInfo().steps;		
        if (stepCount == null) {   
            stepCount = 0;
        }

        dc.setColor(displayColor, Graphics.COLOR_TRANSPARENT);
        dc.drawText(
            x,
            y,
            mediumFont,
            stepCount.format("%d"),
            Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER
        );
        var stepsHeight = Graphics.getFontHeight(mediumFont);
        dc.drawText(
            x,
            y + (stepsHeight / 2),
            iconsFont,
            "s",
            Graphics.TEXT_JUSTIFY_CENTER
        );
    }

    private function drawHeartRateDisplay(dc) {
        x = screenWidth / 4 + 30;
        y = screenHeight / 4 * 3;

        var heartRate = Activity.getActivityInfo().currentHeartRate;
        if (heartRate == null) {
            heartRate = "--";
        } else {
            heartRate = heartRate.format("%d");
        }

        dc.setColor(displayColor, Graphics.COLOR_TRANSPARENT);
        dc.drawText(
            x,
            y,
            mediumFont,
            heartRate,
            Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER
        );
        var heartRateHeight = Graphics.getFontHeight(mediumFont);
        dc.setColor(Graphics.COLOR_RED, Graphics.COLOR_TRANSPARENT);
        dc.drawText(
            x,
            y + (heartRateHeight / 2),
            iconsFont,
            "h",
            Graphics.TEXT_JUSTIFY_CENTER
        );
    }
}

